# CityBuilder

This project allows the reconstruction and visualization in 3D of buildings in Qgis.

The plugin is based on several softwares:

- First of all Geoflow which is the main support of this plugin  
    Geoflow is a software developed by Dutch researchers that takes as input two main data sources: the BAG data (BAG being the most detailed building and address register available in open source in the Netherlands), and the AHN data (digital elevation map of the Netherlands). From these two data sources, geowflow will reconstruct these buildings in 3D, after which these buildings will be visualized in 3D Bag which is a 3D viewer and which provides a 3D model of the Netherlands.
- This project also uses CityJSON Loader which is a Qgis plugin to load CityJSON datasets.

Downloading these two modules is necessary in order to use the CityBuilder plugin.

## TYPES OF DATA TAKEN IN BY THE CityBuilder PLUGIN.

This plugin takes two types of data as input:

- Cadastral data in shapefile (shp) format.
- Lidar data in .las format

Below are links to the site where you can get these datasets.

- For the French cadastral data: https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/shp/
- For Lidar data: https://geoservices.ign.fr/lidarhd

The plugin also accepts the Geopackage (gpkg) format for cadastral data.

## HOW TO USE THE CityBuilder PLUGIN ?

## Installation

To get the source code and put it directly in a Qgis plugin, go to the Gitlab page.  
Be careful, This plugin was tested with QGIS 3.28 on Windows. Note that is does NOT work on 3.30.

For this plugin to work, you need to install two other softwares.

### Install Geoflow

go to the following link: https://github.com/geoflow3d/geoflow-bundle/releases

We have done this project with this version of Geoflow, a version recommended for Windows users : download Geoflow-cli-2022.06.17-win64.exe.

### Install the Qgis CityJSON Loader plugin

From Qgis, go to the "Extensions" tab then look for the "CityJSON Loader" plugin and install it.

### Install the CityBuilder plugin

#### Install the plugin

1. download the plugin CityBuilder.zip below
2. open QGIS go to the tab Extension < Install / manage extensions < install from a ZIP
3. Load the path where you placed the ZIP.
4. Click on Install plugin

![ZIP](ZIP.png)

#### What you need to change

1. Make sure you have access to the different libraries used here in the Python part of your QGIS. If necessary : `pip install geopandas` in the Python Console and, if it didn't work : run it in a terminal from your QGIS file.
2. You also have to download the file flowchart and put it somewhere easily accessible in a terminal. Remember the path of that file and put it instead of "C:/geoDev" in the python script :
3. Then, in the file flowchart that we provide, take the path of the reconstruct.json file and put it instead of C:/geoDev/reconstruct.json in here :
    ![Ligne de commande](pictures/ligne_commande.png)

#### Warnings

Note that if you don't have the same path for the geof.exe of Geoflow, you also have to change it (twice).

Here we use && to make both of the command prompts, so you may have to change the && if it isn't the way it works on your computer. (Brief explanation : we change the directory of the terminal so that Geoflow recognise the flowchart runner.json that have to be associated with the reconstruct.json file)

Note that in all path files, we have some / and not .

## Use

### JSON generation

Click on CityBuilder:

- add the shapefile which will be used as footprint
- add the point cloud in .las format
- Choose the location where you want to save your JSON file

### Visualization of the JSON with the CityJSON Laoder plugin

Click on CityJSON Loader:

- add the JSON file you have previously generated
- check the box Split Layers according to object type
- check the box Load semantic surfaces (so that you have all the Lod differentiated in the display of your building)

![CityJson_Loader](pictures/cityJson_Loader.png)

Then go to the View tab < 3D View < New 3D Map View. You will be able to see your reconstructed building in 3D.

![3D_View](pictures/vue_carto_3D.png)

## License

Distributed under the terms of the [GPLv2+ license](LICENSE).
