# -*- coding: utf-8 -*-
"""
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import os
import json
import numpy as np
import geopandas

from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QFileDialog, QMessageBox, QLineEdit, QCheckBox
from qgis.core import QgsVectorLayer, QgsMapLayer, QgsProject, QgsPointCloudLayer


# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
from .BuildingModelBuilder_dialog import BuildingModelBuilderDialog
import os.path


class BuildingModelBuilder:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        self.shp_filename = None
        self.las_filename = None
        self.out_filename = None
        
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'BuildingModelBuilder_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&BuildingModelBuilder')

        # Check if plugin was started the first time in current QGIS session
        # Must be set in initGui() to survive plugin reloads
        self.first_start = None

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('BuildingModelBuilder', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            # Adds plugin icon to Plugins toolbar
            self.iface.addToolBarIcon(action)

        if add_to_menu:
            self.iface.addPluginToVectorMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/BuildingModelBuilder/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'BuildingModelBuilder'),
            callback=self.run,
            parent=self.iface.mainWindow())

        # will be set False in run()
        self.first_start = True
        
        

    ### --- INPUTS SHP & LAS --- ###
    def select_input_shp(self):
        filename, _filter = QFileDialog.getOpenFileName(
            self.dlg, "Select input file","","Shapefile (*.shp)")
        self.dlg.lineshp.setText(filename)
        self.shp_filename = filename
        #QMessageBox.information(self.iface.mainWindow(), "Titre de la boîte de dialogue", self.shp_filename)
        if not filename or filename=='':
            return
        macouche = QgsVectorLayer(filename, "FootPrint",'ogr')
        if not macouche or not macouche.isValid():
            QMessageBox.warning(self.iface.mainWindow(), "Echec",
            "Le fichier \"%s\"n'est pas reconnu par Qgis:"% filename.replace('/',os.sep))
            return
        QgsProject.instance().addMapLayer(macouche)
        
    def select_input_las(self):
        filename, _filter = QFileDialog.getOpenFileName(
            self.dlg, "Select input file","","LAS files (*.las)")
        self.dlg.linelas.setText(filename)
        self.las_filename = filename
        if not filename or filename=='':
            return
        macouche = QgsPointCloudLayer(filename, "PointCloud",'pdal')
        if not macouche or not macouche.isValid():
            QMessageBox.warning(self.iface.mainWindow(), "Echec",
            "Le fichier \"%s\"n'est pas reconnu par Qgis:"% filename.replace('/',os.sep))
            return filename
        QgsProject.instance().addMapLayer(macouche)
        
        
    ### --- OUTPUTS --- ###
    def save_output_model(self):
        filename, _filter = QFileDialog.getSaveFileName(
            self.dlg, "Save output file","","Tous les fichiers")
        self.dlg.linemodel.setText(filename)
        self.out_filename = filename
        return 

    def construction(self):
        # Get the filenames and path from the object's attributes
        fp = self.shp_filename  # Shapefile filename
        pc = self.las_filename  # Point cloud filename
        path = self.out_filename  # Output directory path
    
        if self.dlg.checkBox.isChecked():
        # If the checkbox is checked, execute this block of code
        
            # Command to run Geoflow with runner.json configuration file
            cmd = "cd C:/geoDev && C:/Geoflow/bin/geof.exe runner.json"
            cmd += " --INPUT_FOOTPRINT_SOURCE=" + fp
            cmd += " --INPUT_LAS_FILES=" + pc
            cmd += " --OUTPUT_CITYJSON_DIR=" + path
            os.system(cmd)  # Run the command
        
            file = path + "/1.json"  # Path to the generated JSON file
            if os.path.exists(file):
                with open(file, 'r') as f:
                    data = json.load(f)  # Load the JSON data

                data['metadata']['referenceSystem'] = "EPSG::2154"  # Update the reference system

                vertices = np.array(data['vertices'])  # Get the vertices array
                hauteurs = vertices[:, 2].astype(int)  # Get the z-coordinates

                base = np.unique(hauteurs)[1]  # Find the base height

                hauteurs = np.where(hauteurs != 0, hauteurs - int(base), 0)  # Adjust heights relative to base
                vertices[:, 2] = hauteurs  # Update the z-coordinates

                data['vertices'] = vertices.tolist()  # Update the vertices in the JSON data

                with open(file, 'w') as f:
                    json.dump(data, f, indent=4, ensure_ascii=False, default=lambda o: o.tolist() if isinstance(o, np.ndarray) else o)
                    # Save the modified JSON data
            
        else:
        # If the checkbox is not checked, execute this block of code
        
            def inv_com(idx, fp, pc, path):
            # Function to invoke Geoflow with reconstruct.json configuration file
                fp_select = str(idx)
                json_file = path + fp_select + ".json"  # Path to the output JSON file

                cmd = "C:/Geoflow/bin/geof.exe C:/geoDev/reconstruct.json"
                cmd += " --input_footprint=" + fp
                cmd += " --input_pointcloud=" + pc
                cmd += " --output_cityjson=" + json_file + " --input_footprint_select=" + fp_select

                os.system(cmd)  # Run the command to invoke Geoflow
                shiftDown(json_file)  # Apply shiftDown function to the generated JSON file

                return

            def shiftDown(file):
            # Function to adjust heights in the JSON file
                if os.path.exists(file):
                    with open(file, 'r') as f:
                        data = json.load(f)  # Load the JSON data

                    data['metadata']['referenceSystem'] = "EPSG::2154"  # Update the reference system

                    der_bound = data['CityObjects']["1"]["geometry"][0]["boundaries"][0][0][-1]  # Get the last boundary index

                    base_value = data['vertices'][der_bound + 1][2]  # Get the base value
                    new_verti = [v[2] - base_value if i > der_bound else v[2] for i, v in enumerate(data['vertices'])]
                    # Calculate new vertex heights relative to base value

                    for i, vertex in enumerate(data['vertices']):
                        vertex[2] = new_verti[i]  # Update the z-coordinate of each vertex

                    attributes = data['CityObjects']['1']['attributes']  # Get the attributes object
                    attributes['ArrDissolve-LoD12.global_elevation_50p'] = attributes['ArrDissolve-LoD12.global_elevation_50p'] - attributes[
                        'LASInPolygons.ground_elevations']  # Adjust attribute values
                    attributes['ArrDissolve-LoD12.global_elevation_70p'] = attributes['ArrDissolve-LoD12.global_elevation_70p'] - attributes[
                        'LASInPolygons.ground_elevations']
                    attributes['ArrDissolve-LoD12.global_elevation_max'] = attributes['ArrDissolve-LoD12.global_elevation_max'] - attributes[
                        'LASInPolygons.ground_elevations']
                    attributes['ArrDissolve-LoD12.global_elevation_min'] = attributes['ArrDissolve-LoD12.global_elevation_min'] - attributes[
                        'LASInPolygons.ground_elevations']
                    attributes['LASInPolygons.ground_elevations'] = 0  # Set ground elevations to 0

                    with open(file, 'w') as f:
                        json.dump(data, f)
                        # Save the modified JSON data
                return

            def create_bat(fp, pc, path):
            # Function to create the batch process for multiple shapefile features
                shp = geopandas.read_file(fp)  # Read the shapefile

                if shp.shape[0] == 1:
                    inv_com(1, fp, pc, path)  # Invoke Geoflow for a single shapefile feature
                else:
                    for i in range(1, shp.shape[0]):
                        inv_com(i, fp, pc, path)  # Invoke Geoflow for each shapefile feature
                return

            create_bat(fp, pc, path)  # Call the create_bat function to initiate the batch process

        return
        
    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginVectorMenu(
                self.tr(u'&BuildingModelBuilder'),
                action)
            self.iface.removeToolBarIcon(action)


    def run(self):
        """Run method that performs all the real work"""

        # Create the dialog with elements (after translation) and keep reference
        # Only create GUI ONCE in callback, so that it will only load when the plugin is started
        if self.first_start == True:
            self.first_start = False
            self.dlg = BuildingModelBuilderDialog()
            self.dlg.pushshp.clicked.connect(self.select_input_shp)
            self.dlg.pushlas.clicked.connect(self.select_input_las)
            self.dlg.pushmodel.clicked.connect(self.save_output_model)
            self.dlg.calcul.clicked.connect(self.construction)
            
            
            # self.dlg.pushok.clicked.connect(self.construction)

        # show the dialog
        self.dlg.show()

        # Run the dialog event loop
        result = self.dlg.exec_()
 
        # See if OK was pressed
        if result:

            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            pass
