# Contributors

The following people have contributed to CityBuilder:

* Arnaud Breillad
* Louis Steinmetz
* Mathéo Maréchal
* Maud Brossard
* [Thomas Muguet](https://gitlab.com/tmuguet)
